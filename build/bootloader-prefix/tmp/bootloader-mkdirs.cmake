# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "C:/Users/becer/esp/esp-idf/components/bootloader/subproject"
  "C:/Users/becer/Documents/ESP32/files_required/program/build/bootloader"
  "C:/Users/becer/Documents/ESP32/files_required/program/build/bootloader-prefix"
  "C:/Users/becer/Documents/ESP32/files_required/program/build/bootloader-prefix/tmp"
  "C:/Users/becer/Documents/ESP32/files_required/program/build/bootloader-prefix/src/bootloader-stamp"
  "C:/Users/becer/Documents/ESP32/files_required/program/build/bootloader-prefix/src"
  "C:/Users/becer/Documents/ESP32/files_required/program/build/bootloader-prefix/src/bootloader-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "C:/Users/becer/Documents/ESP32/files_required/program/build/bootloader-prefix/src/bootloader-stamp/${subDir}")
endforeach()

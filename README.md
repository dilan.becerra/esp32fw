# ESP MQTT and UART communication protocol

This project is based on the repository https://github.com/espressif/esp-idf/tree/master/examples/protocols/mqtt/ssl_mutual_auth, it uses ESP-MQTT library which implements mqtt client to connect to mqtt broker. Added a protocol to communicate with another embedded system via uart so you can get data from that system and send it to AWS-MQTT.

## Hardware Required

This example can be executed on any ESP32 board but it is necessary to load the program in https://gitlab.com/dilan.becerra/temperature_reading_system.git to a stm32-disc1 development board to perform the complete communication. In addition to making the correct connection of the uart pins on both development boards.

## How to use

To use it, just make the connections of both development boards correctly and from the AWS page you will see how the temperature is updated every 30 seconds, in the same way you can make data or firmware requests from the page.
